'''
user interface module
'''
# TODO: Save users other words and only re prompt for the ones that are incorrect.
# todo: Fix Error.
#! Error when width and height are different.

#get user grid size. returns size
def getSize():
    size = 0
    validInput = False

    while(not validInput):

        size = input("Enter size of word find: ")

        #validate input
        if(size.isdigit()):
            validInput = True
        else:
            print("Invalid input: size was not specified as number!")

    return int(size)


#get user words input (words to input into the word find). returns array of words the user entered
def getWords(gridSize):
    resWordsArr = []
    currentWord = ""

    foundWordError = False
    validInput = False

    while(not validInput):
        # reset variables for next iteration (in case foundWordError is True)
        foundWordError = False

        # get user input
        words = input("Enter words seperated by commas: toyota, honda, bmw, nissan.\n: ") # string words

        #validate input (method of input: e.g. truck, van, bus
        for i in words:
            if(i == "," or i == "."):
                # add current word to array
                resWordsArr.append(currentWord)

                currentWord = "" # reset current word

            elif(i == " "):
                # Don't put spaces in current word
                pass
            else:   # Assuming i is part of a word
                currentWord += i

        #validate user word input based on size entered
        for i in resWordsArr:
            if(len(i) > gridSize):
                print("Invalid input. The size of a word entered is greater than specified word find size!") #todo: add option to change grid size here (e.g. would you like to change board size)
                foundWordError = True
                break   # Once found a word with greater length (i.e. word error) we can exit the for loop
    
        if(not foundWordError):
            validInput = True

    return resWordsArr
