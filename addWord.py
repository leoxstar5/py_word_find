# attemp to place a word in a given direction. Returns true if word was successfully placed.
def attemptAddWord(grid, word, gridSize, Direction, position):
    
    # Check if word can be placed in direction in the given position
    if(len(word) <= gridSize):
        # check that every letter of word will fit in the grid

        # Local variables
        currentPos = [0, 0]
        currentPos[0] = position[0]
        currentPos[1] = position[1]
        pos = [0, 0]
        advanceIn = [0, 0]  # x = -1, 0, 1    y = -1, 0, 1   advanced in cell of grid


        # set advanceIn
        if(Direction == Direction.up):          advanceIn = [-1,  0]
        elif(Direction == Direction.down):      advanceIn = [1,   0]
        elif(Direction == Direction.right):     advanceIn = [0,   1]
        elif(Direction == Direction.left):      advanceIn = [0,  -1]
        elif(Direction == Direction.upRight):   advanceIn = [-1,  1]
        elif(Direction == Direction.upLeft):    advanceIn = [-1, -1]
        elif(Direction == Direction.downRight): advanceIn = [1,   1]
        elif(Direction == Direction.downLeft):  advanceIn = [1,  -1]

        for i in range(len(word)):
            # advance in direction
            # check if letter placed in a position thats out of bounds
            if(
            (currentPos[0] >= gridSize or currentPos[1] >= gridSize)# (currentPos[0] >= gridSize[0] or currentPos[0] >= gridSize[1] or currentPos[1] >= gridSize[0] or currentPos[1] >= gridSize[1])
            or 
            (currentPos[0] < 0 or currentPos[1] < 0)):
                return False # word is out of bounds

            # note that advance in(next line) is after checking if out of bounds since the first letter position is before advancing (original position passed as an argument)
            currentPos[0] += advanceIn[0]
            currentPos[1] += advanceIn[1]

        # set pos
        pos[0] = position[0]
        pos[1] = position[1]

        # word fits within grid
        # check either empty '#' or matching letter to word
        for i in word:
            if(not(grid[pos[0]][pos[1]] == '#' or grid[pos[0]][pos[1]] == i)):
                return False    # different 'letter' filled. (letter that's not matching letter(i) in word)

            pos[0] += advanceIn[0]
            pos[1] += advanceIn[1]

        
        # reset pos
        # pos = position   ERROR copies address of position. i.e. pos var is the same var as position so if you change the pos var then you change position var
        pos[0] = position[0]
        pos[1] = position[1]

        # no return False were executed then place the word in grid
        for i in word:
            # Place letter
            grid[pos[0]][pos[1]] = i

            # advance for next letter placing
            pos[0] += advanceIn[0]
            pos[1] += advanceIn[1]
            
        return True

    else:
        return False # word exceeds size of x or y dimension of grid