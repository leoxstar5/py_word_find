'''
Project: Word Find Generator

details: This program gets a user specified grid, and a list of words to create a word find.

#TODO's:
#TODO: Implement GUI. for the result let the user customize the wordfind and be able to print it.

#TODO: Mark the word in list of words that cannot be placed in grid
#Todo: Use grid and variables in a class pass reference to obj (instead of passing multiple references to variables for each function).
#Todo: show uppercase of first letter of each word in key. (easier to see key)
#Todo: Make it less rare for word overlap. (potential solution: make each word get tested for more directions and placings and add that at least x amount of words should have an overlap (add check if overlap))

BUG: if you choose x different than y for grid dimensions usually you end up in an ERROR: index out of range.
'''

#imports
from array import *

# user module
import user

# functions module
import functions

if __name__ == "__main__":

    # get size
    gridSize = user.getSize()

    # create grid
    grid = functions.createGrid('#', gridSize, gridSize)

    # Print the grid array
    functions.printGrid(grid)

    # get words
    gridWords = user.getWords(gridSize) # gridWords = []
    print("You entered: " + str(gridWords))

    # put words in grid
    for word in gridWords:
        functions.addWord(grid, word, gridSize)

    # print key
    print("\nKey:")
    functions.printGrid(grid)

    # print Word Find
    print("\nWord Find:")
    
    functions.fillEmptySpots(grid)

    functions.printGrid(grid)