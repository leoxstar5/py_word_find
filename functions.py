# imports
import random

from addWord import *

# Enum direction class
from enum import Enum

class Dir(Enum):
    # vertical horizontal
    up = 1
    down = 2
    right = 3
    left = 4   
    
    # diagonal
    upRight = 5
    upLeft = 6
    downRight = 7
    downLeft = 8

def hasOpenSpot(grid):  # has at least one open spot
    for i in grid:
        for j in i:
            if(j == '#'):
                return True
    return False

# Create dynamic grid specified by user. src: https://stackoverflow.com/questions/2397141/how-to-initialize-a-two-dimensional-array-in-python/44382900#44382900
def createGrid(content, x, y):
    return [ [content]*x for i in range(y)]


def printGrid(grid): # prints a 2d array
    for r in grid:
        for c in r:
            print(c, end = " ")
        print()


def addWord(grid, word, gridSize):
    
    count = 0 # Keep track of tries

    # set random position
    currentXRand = random.randint(0, (gridSize - 1))
    currentYRand = random.randint(0, (gridSize - 1))
    
    # Create random enum: Direction obj
    randDir = Dir(1)
    
    
    placedWord = False
    RandDirIsValid = False
    
    ##### Random direction #####

    dirArr = []
    # populate dirArr
    for Direction in Dir:
        dirArr.append(Direction)

    # randomly pick a direction
    randDir = random.randint(0, 7)

    # Simulating a do while loop in python
    while True:
        
        # call attemptAddWord
        if(attemptAddWord(grid, word, gridSize, dirArr[randDir], [currentXRand, currentYRand])):
            placedWord = True
            break   # since we don't need other directions to be tested
        else:
            if(hasOpenSpot(grid)):

                # change random direction
                randDir = random.randint(0, 7)

                #change random spot
                currentXRand = random.randint(0, (gridSize - 1))
                currentYRand = random.randint(0, (gridSize - 1))
        
        
        count += 1

        if(placedWord or not hasOpenSpot(grid)):    #Todo: if word not added to grid then notify user. (ideal to try to add word again)
            break

    if(count > 1000): # debug
        print("ERROR exceeded 1000 tries of different directions and placings!!!")


def fillEmptySpots(grid):
    aToZ = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
    randLetter = ""

    for i in range(len(grid)):
        for j in range(len(grid[0])):
            if(grid[i][j] == '#'):

                randLetter = aToZ[random.randint(0, len(aToZ)) -1]
                grid[i][j] = randLetter